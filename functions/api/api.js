const express = require("express");
const bodyParser = require("body-parser");
const serverless = require("serverless-http");
const fs = require("fs");

const app = express();
const router = express.Router();

router.get('/transactions/:trnsId', (req, res) => {
  const trnsId = req.params.trnsId;
  const filepath = `./data/users/${trnsId}.json`;
  fs.readFile(require.resolve(filepath), 'utf8', function (err, data) {
    if (err) throw err;
    res.json(JSON.parse(data));
  });
});

router.get('/quotes', (req, res) => {
  res.json({quote: "pending"});
});

app.use("/v1/", router);

app.use(bodyParser.json());

module.exports.handler = serverless(app);
